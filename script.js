console.log("Wazzup");

//3 and 4
let num = prompt(`Give me a number to get a Cube:`)
const getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}.`);


//5 and 6
let address = ["123 Dimakita Ave ZX", "Pilipinas 12345"]
const [street, country] = address

console.log(`I live at ${street}, ${country}.`);


//7, 8 and 9
let animal = {
	name: "Lolong",
	type: "Saltwater Crocodile",
	weight: 1075,
	measurement: "20 ft 3 in "
};

const {name, type, weight, measurement} = animal;
console.log(`${name} was a ${type}, He weighed at ${weight} kgs with a measurement of ${measurement}.`);


//9 and 10
let numbers = [1, 2, 3, 4, 5];
const [num1, num2, num3, num4, num5] = numbers

numbers.forEach( number => console.log(number));


//11
let reduceNumber = [1, 2, 3, 4, 5];

const sum = reduceNumber.reduce((a, b) => a + b);
console.log(sum);


//12 and 13
class Dog {
	constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
	}
}
const myDog = new Dog();
myDog.name = "Harper";
myDog.age = 2;
myDog.breed = "Siberian Husky";
console.log(myDog);

const myNewDog = new Dog("Spy", 4, "Miniature Pinscher");
console.log(myNewDog);

